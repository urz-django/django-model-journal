#!/usr/bin/env python

from setuptools import setup, find_packages

setup(name='django-model-journal',
    version='0.2',
    description='Provides a simple model history, including all changed attributes. Does not require schema changes, just one additional database table.',
    author='Daniel Klaffenbach',
    author_email='daniel.klaffenbach@hrz.tu-chemnitz.de',
    url='https://gitlab.hrz.tu-chemnitz.de/urz-django/django-model-journal/',
    packages=find_packages(exclude=["contrib", "tests"]),
    package_data = {
        'model_journal': [
            'templates/model_journal/*.html',
        ]
    },
    python_requires='>=3.6',
    install_requires=[
        'django>=3.2',
    ],
)
