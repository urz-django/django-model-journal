#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function, unicode_literals
import subprocess

"""
Generates a GitLab CI config from the tox.ini so that every
tox environment runs in a seprate GitLab CI job.
"""

YAML_TEMPLATE = """\
stages:
    - test

# tox location on FU_URZ_CI_SERVER
variables:
  TOX: "/opt/venv/python3.6/tox/bin/tox"
  LD_LIBRARY_PATH: "/opt/sqlite-compat-django/lib/"
"""


def get_tox_envs():
    p = subprocess.Popen(["tox", "-l"], stdout=subprocess.PIPE)
    _stdout, _stderr = p.communicate()
    return _stdout.decode("utf-8").splitlines()


def generate_gitlab_ci_yaml():
    ci_config = YAML_TEMPLATE
    for tox_env in get_tox_envs():
        ci_config += "%s:\n" % tox_env
        ci_config += "    stage: test\n"
        ci_config += "    script: $TOX -e '%s'\n\n" % tox_env
    return ci_config


if __name__ == '__main__':
    with open('.gitlab-ci.yml', 'w') as f:
        f.write(generate_gitlab_ci_yaml())
