# Generated by Django 3.2.14 on 2023-11-07 07:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('model_journal', '0002_auto_20180502_0816'),
    ]

    operations = [
        migrations.AlterField(
            model_name='journalentry',
            name='attrs',
            field=models.JSONField(editable=False),
        ),
    ]
