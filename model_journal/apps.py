from django.apps import AppConfig


class ModelJournalConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'model_journal'
