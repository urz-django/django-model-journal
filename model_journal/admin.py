from django.contrib import admin
from django.db.models import CharField, ForeignKey, Q
from django.db.models.functions import Cast
from .models import JournalEntry


class JournalAdmin(admin.ModelAdmin):
    """
    This class can be used for all models with a Journal enabled. It
    provides a custom history view, which displays all `JournalEntry`
    records for a model and inline models.
    """
    object_history_template = 'model_journal/object_history.html'
    
    def history_view(self, request, object_id, extra_context=None):
        if not hasattr(self.model, '_MODEL_JOURNAL_ATRR'):
            return super().history_view(request, object_id, extra_context=extra_context)

        # Finds all JournalEntries for `self.model`
        obj = self.get_object(request, object_id)
        related_journal_manager = getattr(obj, obj._MODEL_JOURNAL_ATRR)
        
        journal_filter = Q(
            Q(object_id=object_id) &
            related_journal_manager.get_content_type_filter(with_children=True, with_parents=True)
        )
        
        """
        We also want to show the journal entries of all models displayed through
        inline instances. So loop through the inline instances, find all models
        with a `Journal` relation and adjust the journal_filter accordingly. 
        """ 
        for inline in self.get_inline_instances(request):
            model = inline.model
            if not hasattr(model, '_MODEL_JOURNAL_ATRR'):
                # This model does not have a Journal relation
                continue
            
            # Find the name of the ForeignKey of the related/inline model.
            fk_name = inline.fk_name
            if not fk_name:
                # If `fk_name` was not set in the inline admin, try to guess it 
                for field in model._meta.fields:
                    if isinstance(field, ForeignKey) and field.related_model == self.model:
                        fk_name = field.name
                        break
            if not fk_name:
                continue

            # JournalEntry.object_id is a CharField and some databases (e.g. PostgreSQL) are picky when values
            # from IntegerFields are passed in. This is why we need to cast the objects' pk into a string.
            related_pks = model.objects.filter(
                **{fk_name: object_id}
            ).annotate(
                pk=Cast('id', CharField(max_length=255))
            ).values_list('pk', flat=True)

            # Finds all JournalEntries for `inline.model`
            inline_filter = Q(Q(object_id__in=related_pks) &
                     Q(content_type__app_label=model._meta.app_label) &
                     Q(content_type__model=model.__name__.lower()))
            journal_filter = journal_filter | inline_filter
        
        journal = JournalEntry.objects.filter(journal_filter).order_by('-id')
        
        extra_context = {
            'journal': journal
        }
        return super().history_view(request, object_id, extra_context=extra_context)
