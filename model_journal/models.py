import datetime
from getpass import getuser
import logging
from django.apps import apps
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation, FieldDoesNotExist
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ImproperlyConfigured
from django.core.serializers.json import DjangoJSONEncoder
from django.db import models
from django.db.models import Q
from django.utils.formats import date_format
from django.utils.html import format_html
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _, gettext
from . import _thread_local_storage

logger = logging.getLogger(__name__)


class JournalEntryManager(models.Manager):
    def get_content_type_filter(self, with_children=False, with_parents=False):
        if not hasattr(self, 'instance'):
            return None

        content_type_filter = Q(
            Q(content_type__app_label=self.instance._meta.app_label) &
            Q(content_type__model=self.instance.__class__.__name__.lower())
        )

        if with_children:
            for cls in apps.get_models():
                if issubclass(cls, self.instance.__class__) and cls != self.instance.__class__:
                    content_type_filter |= Q(
                        Q(content_type__app_label=cls._meta.app_label) &
                        Q(content_type__model=cls.__name__.lower())
                    )

        if with_parents:
            for cls in self.instance._meta.get_parent_list():
                content_type_filter |= Q(
                    Q(content_type__app_label=cls._meta.app_label) &
                    Q(content_type__model=cls.__name__.lower())
                )
        return content_type_filter

    def with_children(self):
        """
        Useful for related managers to get the full journal for the model,
        in case model inheritance is used.
        """
        if not hasattr(self, 'instance'):
            return self.all()
        else:
            content_type_filter = self.get_content_type_filter(with_children=True)
            return self.model.objects.filter(object_id=self.instance.pk).filter(content_type_filter)

    def with_parents(self):
        """
        Useful for related managers to get the full journal for the model,
        in case model inheritance is used.
        """
        if not hasattr(self, 'instance'):
            return self.all()
        else:
            content_type_filter = self.get_content_type_filter(with_parents=True)
            return self.model.objects.filter(object_id=self.instance.pk).filter(content_type_filter)


class JournalEntry(models.Model):
    """
    Contains all logs for all models. The objects are referenced through a
    GenericForeignKey and the attributes are stored in a JSON field.
    """
    OPERATION_ADD = 1
    OPERATION_CHANGE = 2
    OPERATION_DELETE = 3

    OPERATION_CHOICES = (
        (OPERATION_ADD, _('Created')),
        (OPERATION_CHANGE, _('Changed')),
        (OPERATION_DELETE, _('Deleted'))
    )

    user = models.CharField(db_index=True, editable=False, max_length=100)
    time = models.DateTimeField(auto_now_add=True, editable=False)
    operation = models.PositiveSmallIntegerField(choices=OPERATION_CHOICES, db_index=True, editable=False)

    content_type = models.ForeignKey(ContentType, editable=False, on_delete=models.CASCADE)
    object_id = models.CharField(db_index=True, editable=False, max_length=255)
    instance = GenericForeignKey('content_type', 'object_id')

    attrs = models.JSONField(editable=False, encoder=DjangoJSONEncoder)

    objects = JournalEntryManager()

    class Meta:
        default_manager_name = 'objects'
        ordering = ('id',)

    def __str__(self):
        return "JournalEntry from %s" % date_format(self.time, "SHORT_DATETIME_FORMAT")

    def _get_last_entry(self):
        """
        Returns the predecessor of this JournalEntry.
        
        :rtype: JournalEntry | None
        """
        return self.__class__.objects.filter(content_type=self.content_type, object_id=self.object_id,
                                             pk__lt=self.pk).last()

    def get_diff(self):
        last = self._get_last_entry()

        if last:
            attrs_last = last.attrs
        else:
            attrs_last = {}

        diff = {}
        for attr in self.attrs:
            if attr not in attrs_last:
                diff[attr] = (None, self.attrs[attr])
            elif attrs_last[attr] != self.attrs[attr]:
                diff[attr] = (attrs_last[attr], self.attrs[attr])
        return diff

    def get_changed_attributes(self):
        if self.operation == self.OPERATION_ADD:
            return []

        last = self._get_last_entry()
        if not last:
            return []
        diff = []
        for attr in self.attrs:
            if attr not in last.attrs:
                diff.append(attr)
            elif last.attrs[attr] != self.attrs[attr]:
                diff.append(attr)
        return diff

    def get_display_short(self):
        if self.operation == self.OPERATION_CHANGE:
            attrs = ', '.join(self.get_changed_attributes())
            if not attrs:
                return gettext('%s was saved without any changes.' % self.instance._meta.verbose_name)
            else:
                return gettext('Changed attributes: %s' % attrs)
        elif self.operation == self.OPERATION_ADD:
            return gettext('%s was added.' % self.instance._meta.verbose_name)
        else:
            return gettext('%s was deleted.' % self.instance._meta.verbose_name)

    def get_display_full(self):
        verbose_name = self.instance._meta.verbose_name

        value_getter_method = "get_%s_display_value" % self.instance._MODEL_JOURNAL_ATRR
        value_getter = getattr(self.instance, value_getter_method, lambda x, y: y)

        if self.operation == self.OPERATION_ADD:
            label = gettext('%s was added.' % verbose_name)
            li = ''
            for attr in self.attrs:
                if self.attrs[attr]:
                    try:
                        nam = self.instance._meta.get_field(attr).verbose_name
                    except FieldDoesNotExist:
                        nam = attr

                    val = value_getter(attr, self.attrs[attr]) or self.attrs[attr]
                    li += format_html('<li><strong>{}:</strong> "{}"</li>', nam, val)
            if li:
                label += '<ul>%s</ul>' % li
            return mark_safe(label)
        elif self.operation == self.OPERATION_CHANGE:
            diff = self.get_diff()
            if not diff:
                return gettext('%s was saved without any changes.' % self.instance._meta.verbose_name)
            else:
                label = gettext('%s was changed:' % verbose_name)
                li = ''
                for attr in diff:
                    try:
                        nam = self.instance._meta.get_field(attr).verbose_name
                    except FieldDoesNotExist:
                        nam = attr

                    val_before = value_getter(attr, diff[attr][0]) or diff[attr][0]
                    val_after = value_getter(attr, diff[attr][1]) or diff[attr][1]
                    li += format_html(
                            '<li><strong>{}:</strong> "{}" &rarr; "{}"</li>', nam, val_before, val_after)
                if li:
                    label += '<ul>%s</ul>' % li
                return mark_safe(label)
        else:
            return gettext('%s was deleted.' % verbose_name)


class Journal(GenericRelation):
    def __init__(self, exclude=[]):
        super(Journal, self).__init__(to=JournalEntry)
        self._exclude = exclude

    def contribute_to_class(self, cls, name):
        super(Journal, self).contribute_to_class(cls, name)

        # Monkey-patch the model class if it does not define a
        # `get_journal_entry_username` method.
        if not hasattr(cls, 'get_journal_entry_username'):
            setattr(cls, 'get_journal_entry_username', get_journal_entry_username)
        else:
            logger.debug("Using get_journal_entry_username() provided by model %s." % cls.__name__)

        if not hasattr(cls, '_MODEL_JOURNAL_ATRR'):
            setattr(cls, '_MODEL_JOURNAL_ATRR', name)
        elif cls._MODEL_JOURNAL_ATRR != name:
            logger.error("Multiple Journal relations defined for model %s." % cls.__name__)
            return

        models.signals.post_save.connect(self.post_save, sender=cls, weak=False)
        models.signals.post_delete.connect(self.post_delete, sender=cls, weak=False)

    def create_journal_entry(self, instance, operation):
        attrs = {}
        for field in instance._meta.fields:
            if field.attname not in self._exclude:
                value = getattr(instance, field.attname)
                # Store date/datetime in ISO-format, as it's not JSON-serializable
                if isinstance(value, datetime.date):
                    value = value.isoformat()
                attrs[field.attname] = value

        JournalEntry.objects.create(
            user=instance.get_journal_entry_username(),
            operation=operation,
            attrs=attrs,
            object_id=instance.pk,
            content_type=ContentType.objects.get_for_model(instance)
        )

    def post_save(self, instance, created, **kwargs):
        if created:
            operation = JournalEntry.OPERATION_ADD
        else:
            operation = JournalEntry.OPERATION_CHANGE
        self.create_journal_entry(instance, operation)

    def post_delete(self, instance, **kwargs):
        self.create_journal_entry(instance, JournalEntry.OPERATION_DELETE)


def get_journal_entry_username(self):
    """
    This function will become a member of all models which have a `Journal`
    relation defined, unless a method with the same name already exists on
    that model.
    
    In the latter case it is possible to override the username, in case you
    do not want to rely on thread-local storage. For example, if you already
    keep track of the modifying user in your model, you could reuse this
    information for the model journal.
    """
    try:
        return getattr(_thread_local_storage, 'model_journal_username')
    except AttributeError:
        if 'model_journal.middleware.ModelJournalMiddleware' not in getattr(settings, 'MIDDLEWARE_CLASSES', []) and \
                        'model_journal.middleware.ModelJournalMiddleware' not in getattr(settings, 'MIDDLEWARE', []):
            raise ImproperlyConfigured(
                "The ModelJournalMiddleware is missing from your MIDDLEWARE_CLASSES"
            )
        else:
            username = getuser()
            logger.debug("Using current system user (%s) for ModelJournal logging." % username)
            return username
