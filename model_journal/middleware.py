from django.utils.deprecation import MiddlewareMixin
from . import _thread_local_storage


class ModelJournalMiddleware(MiddlewareMixin):
    def process_request(self, request):
        username = ''
        if hasattr(request, 'user') and request.user.is_authenticated:
            user = request.user
            username = getattr(user, user.USERNAME_FIELD)

        _thread_local_storage.model_journal_username = username

    def process_response(self, request, response):
        _thread_local_storage.model_journal_username = ''
        return response
