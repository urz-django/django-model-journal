# -*- coding: utf-8 -*-
from __future__ import print_function, unicode_literals
from django.test import TransactionTestCase
from model_journal.models import JournalEntry
from .models import RelatedTestModel, TestModel


class JournalEntryTestCase(TransactionTestCase):
    def setUp(self):
        self.related_object = RelatedTestModel.objects.create(char_field="Foo")

    def _create_test_model(self):
        """
        :rtype: TestModel
        :return: A new TestModel instance
        """
        obj = TestModel()
        obj.char_field = "test"
        obj.int_field = 1
        obj.bool_field = True
        obj.foreign_key = self.related_object
        obj.save()
        return obj

    def test_journal_entry_api(self):
        # We'll start with an empty Journal
        self.assertEqual(JournalEntry.objects.count(), 0)
        # Then we create a new TestModel
        obj = self._create_test_model()
        # And now we should have one JournalEntry record
        self.assertEqual(obj.journal.count(), 1)

        # The JournalEntry operation should be "add"
        entry = JournalEntry.objects.first()
        self.assertEqual(entry.operation, JournalEntry.OPERATION_ADD)

        # Test if all the required attributes are logged correctly
        attrs = entry.attrs
        self.assertIn('char_field', attrs)
        self.assertIn('int_field', attrs)
        self.assertIn('bool_field', attrs)
        self.assertIn('foreign_key_id', attrs)
        self.assertNotIn('created', attrs)

        # Test JSON types
        self.assertEqual(attrs['char_field'], obj.char_field)
        self.assertEqual(attrs['int_field'], obj.int_field)
        self.assertEqual(attrs['bool_field'], obj.bool_field)
        # @attention: ForeignKeys are only logged as ids
        self.assertEqual(attrs['foreign_key_id'], obj.foreign_key_id)

        # Now we'll modify two attributes and see if they show up in the diff
        obj.char_field = "test2"
        obj.int_field = 2
        obj.save()

        self.assertEqual(obj.journal.count(), 2)
        updated_entry = JournalEntry.objects.last()

        self.assertEqual(updated_entry.operation, JournalEntry.OPERATION_CHANGE)

        # We've only modified `char_field` and `int_field`, so this is the diff which *should* be computed
        # by `get_diff()`:
        expected_diff = {
            'char_field': ("test", "test2"),
            'int_field': (1, 2)
        }
        self.assertEqual(updated_entry.get_diff(), expected_diff)

        # Tests if the related manager methods are present
        self.assertGreater(obj.journal.with_children().count(), 1)
        self.assertGreater(obj.journal.with_parents().count(), 1)


    def test_journal_entry_operation(self):
        """ Tests if the `operation` attribute is set correctly."""
        obj = self._create_test_model()
        last_journal = JournalEntry.objects.last()
        self.assertEqual(last_journal.operation, JournalEntry.OPERATION_ADD)

        obj.int_field += 1
        obj.save()
        last_journal = JournalEntry.objects.last()
        self.assertEqual(last_journal.operation, JournalEntry.OPERATION_CHANGE)

        TestModel.objects.all().delete()
        last_journal = JournalEntry.objects.last()
        self.assertEqual(last_journal.operation, JournalEntry.OPERATION_DELETE)
