# -*- coding: utf-8 -*-
from __future__ import print_function, unicode_literals
import uuid
from django.db import  models
from model_journal.models import Journal


class RelatedTestModel(models.Model):
    """
    Model which is used as ForeignKey during testing.
    """
    char_field = models.CharField(max_length=16)


class TestModel(models.Model):
    char_field = models.CharField(max_length=16)
    int_field = models.IntegerField()
    bool_field = models.BooleanField()
    foreign_key = models.ForeignKey(RelatedTestModel, on_delete=models.CASCADE)
    date_time_field = models.DateTimeField(auto_now_add=True)
    uuid_test = models.UUIDField(default=uuid.uuid4)
    # The `created` field will be excluded from the journal for testing
    created = models.DateTimeField(auto_now_add=True, editable=False)

    journal = Journal(exclude=['created'])

