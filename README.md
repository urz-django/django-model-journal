# django-model-journal

[![build status](https://gitlab.hrz.tu-chemnitz.de/urz-django/django-model-journal/badges/master/build.svg)](https://gitlab.hrz.tu-chemnitz.de/urz-django/django-model-journal/commits/master)

This Django app allows you to keep track of any changes made to a
specific model. Whenenver a model is saved a `JournalEntry` is created
which contains a JSON-representation of the saved model state.

This allows you to create diffs between model versions which can be used
for reporting.

## Features

- Drop-in application:
  - No schema changes required
  - Can be enabled on a per-model basis
- Integration into Django-admin (optional)
- Exclude specific fields from journal

## Installation and Configuration

1. Install package through pip:
    ```bash
    # pip install -e git+https://gitlab.hrz.tu-chemnitz.de/urz-django/django-model-journal.git#egg=django-model-journal
    ```
1. Adjust `INSTALLED_APPS` and `MIDDLEWARE` settings:
    ```python
    INSTALLED_APPS = [
        'django.contrib.contenttypes', # required!
        # ...
        # place somewhere in INSTALLED_APPS
        'model_journal',
    ]

    MIDDLEWARE = [
        # ...
        # place somewhere in MIDDLEWARE
        'model_journal.middleware.ModelJournalMiddleware',
    ]
    ```
1. Run `./manage.py migrate`

## Usage

You need to explicitly specify the models you want to add journal support
to. This has to be done in *your* model code:

```python
# sample models.py

from django.db import  models
from model_journal.models import Journal

class MyModel(models.Model):
    example_field = models.CharField(max_length=16)
    created = models.DateTimeField(auto_now_add=True)

    # This enables the journal for your model. All fields are logged, except for `created`
    journal = Journal(exclude=["created"])
```

If you want to display a model's history in the Django admin, make sure
your admin class inherits from `JournalAdmin`:

```python
# sample admin.py

from django.contrib import admin
from model_journal.admin import JournalAdmin
from .models import MyModel

@admin.register(MyModel)
class MyModelAdmin(JournalAdmin):
    pass
```

## Low-level API

You can access an object's journal directly through the object itself:

```python

obj = MyModel.objects.get(pk=1)
# Access all JournalEntry instances for a specific object
for entry in obj.journal.all():
    print("I am a JournalEntry object from %s" % entry.time)

# Get last diff:
journal_entry = obj.journal.last()
print(journal_entry.get_diff())
# Or simply print the changed atributes:
print(journal_entry.get_changed_attributes())
```